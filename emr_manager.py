import boto3
from datetime import date, datetime
import time
from slackclient import SlackClient
import subprocess
import os
import signal
import sys

BOT_NAME = os.environ["BOT_NAME"]
READ_WEBSOCKET_DELAY = float(os.environ["READ_WEBSOCKET_DELAY"])
SLACK_BOT_TOKEN = os.environ["SLACK_BOT_TOKEN"]
SLACK_CHANNEL = os.environ['SLACK_CHANNEL']#'data-science-aws'
INTERNAL_TIMEOUT = int(os.environ["INTERNAL_TIMEOUT"])
EXCLUDED = os.environ["EXCLUDED"]


def get_excluded_clusters(excluded):
    return excluded.split(",")

if os.environ["TEST_MODE"]=="True":
    TEST_MODE = True
else:
    TEST_MODE = False
sc = SlackClient(SLACK_BOT_TOKEN)
original_sigint_handler = signal.getsignal(signal.SIGINT)

client = boto3.client('emr')

def bot_notified(slack):
    if "type" in slack.keys():
        if slack['type'] == 'message':
            if "text" in slack.keys():
                message = slack['text'].lower()#.split(": ")[1].split(" ")[1]
                return message
            else:
                return None
        else:
            return None
    else:
        return None


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""
    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError("Type %s not serializable" % type(obj))


def lambda_handler(event, context):
    response = client.list_clusters(ClusterStates=['STARTING', 'BOOTSTRAPPING', 'RUNNING', 'WAITING'])
    ids = []
    for c in response['Clusters']:
        if c['Name'] in get_excluded_clusters():
            c['Status']['Timeline']['CreationDateTime'] = json_serial(c['Status']['Timeline']['CreationDateTime'])
            if 'ReadyDateTime' in c['Status']['Timeline'].keys():
                c['Status']['Timeline']['ReadyDateTime'] = json_serial(c['Status']['Timeline']['ReadyDateTime'])
            ids.append(c['Id'])

    if sc.rtm_connect():
        print "Bot connected and running!"
        for i in ids:
            def handler(signum, frame):
                sc.api_call("chat.postMessage", text="Hasta la vista baby! Cluster {} is being terminated." \
                            .format(i), channel=SLACK_CHANNEL, as_user=True)
                if TEST_MODE:
                    sys.exit(0)
                else:
                    command = ["./aws", "emr", "terminate-clusters", "--cluster-ids", "{}".format(i)]
                    subprocess.check_output(command, stderr=subprocess.STDOUT)
                sys.exit(0)

            signal.signal(signal.SIGALRM, handler)
            signal.alarm(INTERNAL_TIMEOUT)

            sc.api_call(
                "chat.postMessage",
                text="Cluster {} is about to be terminated. You have 2 minutes to decide to: terminate or override?"
                    .format(i),
                channel=SLACK_CHANNEL,
                as_user=True)
            res = 0
            while res == 0:
                for slack_event in sc.rtm_read():
                    try:
                        if (bot_notified(slack_event) == 'override'):
                            sc.api_call(
                                "chat.postMessage",
                                text="Cluster {} will not be terminated.".format(i),
                                channel=SLACK_CHANNEL,
                                as_user=True)
                            signal.signal(signal.SIGALRM, original_sigint_handler)
                            signal.alarm(0)
                            res = 1
                        elif (bot_notified(slack_event) == 'terminate'):
                            sc.api_call(
                                "chat.postMessage",
                                text="Hasta la vista baby! Cluster {} is being terminated.".format(i),
                                channel=SLACK_CHANNEL,
                                as_user=True)
                            print(bot_notified(slack_event), TEST_MODE)
                            if TEST_MODE:
                                signal.signal(signal.SIGALRM, original_sigint_handler)
                                signal.alarm(0)
                            else:
                                command = ["./aws", "emr", "terminate-clusters", "--cluster-ids", "{}".format(i)]
                                subprocess.check_output(command, stderr=subprocess.STDOUT)
                            res = 1
                    except Exception as e:
                        print e
                time.sleep(READ_WEBSOCKET_DELAY)
            signal.signal(signal.SIGALRM, original_sigint_handler)
            signal.alarm(0)