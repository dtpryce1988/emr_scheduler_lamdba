# EMR SCHEDULER #

This repo contains ALL code and directories needed to change the AWS Lambda code for emr-cluster-manager-function. 

Author: David Pryce

Email: david.pryce@wandera.com

## How to use ##

Make any edits in the `emr_manager.py` file.

Any new modules used MUST be saved into the same directory you can do this by running:

`pip install <module name> -t /full/dir/install/to`

Then you can select all files within directory (not the directory) to be compressed and either upload to Lambda or S3 bucket.

For automatic zip and upload use the `lambda_deploy` script found in the `datasciutil` python module [found here](https://bitbucket.org/snappli-ondemand/datasciutil).

## How it works ##

* When tested or on schedule (7pm UTC M-F) the code checks if there is any EMR cluster active in any way.
* If there are none then nothing happens.
* If there is more than one cluster on it loops through each one and posts a message to a certain DS channel and requests whether to terminate or override.
* Users have at most 5 minutes to respond (or termination occurs regardless) with "@clusterbot terminate|override".

## To do ##
* Make slack notifications user specific (avoid spamming)
* Integrate with fi-manager and deploybot?
* Tools integration
* Make clusterbot more conversational and wider access